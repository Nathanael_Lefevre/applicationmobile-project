# ApplicationMobile-TP3

Prière de consulter le fichier **Application mobile - Projet.pdf** afin de connaitre les détails de l'application.

Veuillez noter qu'une nouvelle version de la recherche a été uploadée dans la branche improvedSearch.

La raison est que la recherche améliorée étant dans une autre branche, c'est la branche master qui a servi à la construction de l'apk. La branche improvedSearch qui était uniquement en local a été uploadée le jour de la soutenance.
