package fr.uavignon.ceri.tp3.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "collectionVersion_table", indices = {@Index(value = {"_id"},
        unique = true)})
public class CollectionVersion {
    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name="_id")
    Float versionNb;

    public CollectionVersion(Float versionNb) {
        this.versionNb = versionNb;
    }

    public Float getVersionNb(){
        return versionNb;
    }
}
