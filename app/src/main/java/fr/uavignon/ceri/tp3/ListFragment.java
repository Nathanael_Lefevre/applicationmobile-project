package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;

public class ListFragment extends Fragment {

    public static final String TAG = ListFragment.class.getSimpleName();

    private ListViewModel viewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;
    private ProgressBar progress;
    private EditText search;



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);
        listenerSetup();
        observerSetup();

    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setListViewModel(viewModel);
        progress = getView().findViewById(R.id.progressList);
        search = getView().findViewById(R.id.searchView);

        //search.setQueryHint("Rechercher");


        FloatingActionButton fab = getView().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("floatingButton", "floating has been pressed. value of search field is : '" + search.getText().toString() + "'" );
                if(search.getText().toString().compareTo("") != 0) {
                    viewModel.setAllItems(filter(viewModel.getAllItemsFromRepo().getValue(), search.getText().toString()));
                }else{
                    viewModel.setAllItems(new MutableLiveData<List<Item>>(viewModel.getAllItemsFromRepo().getValue()));
                }
                adapter.setItemList(viewModel.getAllItems().getValue());
            }
        });
    }

    private void observerSetup() {
        viewModel.getAllItems().observe(getViewLifecycleOwner(),
                items -> {
                    if(viewModel.getSortStrategy().getValue() == "Alphabetical"){
                        Collections.sort(items, new SortAlphabetically());
                    }else if(viewModel.getSortStrategy().getValue() == "ByDate"){
                        Collections.sort(items, new SortByDate());
                    }

                    adapter.setItemList(items);
                });

        viewModel.getSortStrategy().observe(getViewLifecycleOwner(),
            sortStrategy -> {
                List<Item> items = viewModel.getAllItems().getValue();
                Log.d("change_sortStrategy", items == null ? "items is null" : "items is not null");
                if(items != null) {
                    if (sortStrategy.equals("Alphabetical")) {
                        Collections.sort(items, new SortAlphabetically());
                    } else if (viewModel.getSortStrategy().getValue() == "ByDate") {
                        Collections.sort(items, new SortByDate());
                    }

                    adapter.setItemList(items);
                }
            });

        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                bool -> {
                    if (bool != null) {
                        Log.d(TAG, "Loading state has just been updated : " + bool.toString());
                        if (bool) {
                            System.out.println("setting visibility of progressBar to " + View.VISIBLE);
                            progress.setVisibility(View.VISIBLE);
                        } else {
                            progress.setVisibility(View.GONE);
                            System.out.println("setting visibility of progressBar to " + View.GONE);
                        }
                    }
                });

        viewModel.getLoadCollectionMsg().observe(getViewLifecycleOwner(),
            string -> {
                if (string != null && string != ""){
                    Snackbar.make(getView(), string,
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    viewModel.nullLoadCollectionMsg();  // Remet le loadCollectionMsg à null pour ne pas l'afficher à nouveau en changeant de fragment
                }
            });

        viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                throwable -> {
                    if (throwable != null && throwable.getMessage() != null){
                        Snackbar.make(getView(), "Erreur : " + throwable.getMessage(),
                                Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        viewModel.nullWebServiceThrowable();  // Remet le WebServiceThrowable à null pour ne pas l'afficher à nouveau en changeant de fragment
                    }
                });
    }

    private MutableLiveData<List<Item>> filter(List<Item> items, String query) {
        while(items == null);
        query = query.toLowerCase();
        final ArrayList<Item> filteredItems = new ArrayList<Item>();
        for (Item currentItem : items) {
            final String text = currentItem.getName().toLowerCase();
            if (text.contains(query)) {
                filteredItems.add(currentItem);
            }
        }
        return (new MutableLiveData<List<Item>>((List<Item>) filteredItems));
    }

    class SortAlphabetically implements Comparator<Item>
    {
        // Used for sorting in ascending order of
        // roll number
        public int compare(Item a, Item b)
        {
            return a.getName().compareTo(b.getName());
        }
    }

    class SortByDate implements Comparator<Item>
    {
        // Used for sorting in ascending order of
        // roll number
        public int compare(Item a, Item b)
        {
            if(a.getYear() == -1 && b.getYear() == -1){
                return 0;
            }
            if(a.getYear() == -1){
                return 1;
            }
            if(b.getYear() == -1){
                return -1;
            }
            return a.getYear() - b.getYear();
        }
    }

}