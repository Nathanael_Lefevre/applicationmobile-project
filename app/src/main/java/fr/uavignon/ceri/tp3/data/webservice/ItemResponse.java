package fr.uavignon.ceri.tp3.data.webservice;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemResponse {
    public final List<String> categories;
    public final List<String> timeFrame;
    public final String brand;
    public final String year;
    public final String name;
    public final List<String> technicalDetails;
    public final String description;
    public final Map<String, String> pictures;

    ItemResponse(){
        this.categories = null;
        this.timeFrame = null;
        this.brand = null;
        this.year = null;
        this.name = null;
        this.technicalDetails = null;
        this.description = null;
        this.pictures = null;
    }
}
