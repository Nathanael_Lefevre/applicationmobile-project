package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.ItemCTDP;
import fr.uavignon.ceri.tp3.data.ItemRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private ItemRepository repository;
    private MutableLiveData<Item> item;
    private MutableLiveData<ItemCTDP> itemWithCategory;

    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;



    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }


    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }


    public void nullWebServiceThrowable() {
        webServiceThrowable.postValue(null);
    }


    public DetailViewModel (Application application) {
        super(application);
        repository = ItemRepository.get(application);
        item = new MutableLiveData<>();
        itemWithCategory = new MutableLiveData<>();
        isLoading = repository.getIsLoading();
        webServiceThrowable = repository.getWebServiceThrowable();
    }


    public void setItem(String id) {
        /*
        repository.getItem(id);
        item = repository.getSelectedItem();
        */
        repository.getItemCTD(id);
        itemWithCategory = repository.getSelectedItemWithCategory();
    }


    LiveData<ItemCTDP> getItemWithCategory() {
        return itemWithCategory;
    }

    public void loadCollectionIfNecessary(){
        repository.loadCollection();
    }
    /*  A IMPLEMENTER POUR CHARGER UN SEUL ITEM !
    public void updateWeatherCity(){
        repository.loadCollection(item.getValue());
    }
    */

}

