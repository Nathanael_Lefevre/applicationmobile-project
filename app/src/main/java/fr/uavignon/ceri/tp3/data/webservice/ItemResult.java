package fr.uavignon.ceri.tp3.data.webservice;

import android.util.Log;

import java.util.Date;

import fr.uavignon.ceri.tp3.data.Item;

public class ItemResult {

    public final boolean isLoading;
    public final Throwable error;

    public ItemResult(boolean isLoading, Throwable error) {
        this.isLoading = isLoading;
        this.error = error;
    }


    public static void transferInfo(ItemResponse itemUpdateInfo, Item item){
        item.setName(itemUpdateInfo.name);
        item.setDescription(itemUpdateInfo.description);
        item.setYear(itemUpdateInfo.year != null ? Integer.parseInt(itemUpdateInfo.year) : -1);
        item.setBrand(itemUpdateInfo.brand != null ? itemUpdateInfo.brand : "????");

        item.setLastUpdate(new Date().getTime());
    }

}
