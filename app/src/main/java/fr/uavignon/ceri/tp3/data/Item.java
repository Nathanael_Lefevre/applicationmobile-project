package fr.uavignon.ceri.tp3.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Entity(tableName = "item_table", indices = {@Index(value = {"_id"},
        unique = true)})
public class Item {

    public static final String TAG = Item.class.getSimpleName();

    public static final String ADD_ID = "addId";

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="brand")
    private String brand;

    @NonNull
    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="year")
    private int year;

    @ColumnInfo(name="lastUpdate")
    private long lastUpdate; // Last time when data was updated (Unix time)

    /*
    @NonNull
    @ColumnInfo(name="categories")
    private List<String> categories;
    */


    @Ignore
    public Item(@NonNull String id) {
        this.id = id;
    }

    public Item(@NonNull String id, @NonNull String name, @NonNull String description, int year, long lastUpdate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.year = year;
        this.lastUpdate = lastUpdate;
    }


    public String getId(){
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public String getBrand() { return brand; }

    @NonNull
    public String getDescription() {
        return description;
    }

    public int getYear() {
        return year;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public String getStrLastUpdate() {
        Date date = new Date(lastUpdate);
        DateFormat shortDateFormat = DateFormat.getDateTimeInstance(
                DateFormat.SHORT,
                DateFormat.SHORT, new Locale("FR","fr"));

        return shortDateFormat.format(date);
    }


    public void setName(@NonNull String name) {
        this.name = name;
    }

    public void setBrand(String brand) { this.brand = brand; }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
