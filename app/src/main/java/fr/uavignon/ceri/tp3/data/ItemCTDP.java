package fr.uavignon.ceri.tp3.data;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class ItemCTDP {  // Item with Category, TechnicalDetails and Photo
    @Embedded
    public Item item;

    @Relation(
            parentColumn = "_id",
            entityColumn = "itemId"
    )
    public List<Category> categories;

    @Relation(
            parentColumn = "_id",
            entityColumn = "itemId"
    )
    public List<TechnicalDetail> technicalDetails;

    @Relation(
            parentColumn = "_id",
            entityColumn = "itemId"
    )
    public List<Photo> pictures;
}
