package fr.uavignon.ceri.tp3.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "category_table", indices = {@Index(value = {"_id"},
        unique = true)})
public class Category {

    public static final String TAG = Category.class.getSimpleName();

    public static final String ADD_ID = "addId";

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="catName")
    private String name;

    @NonNull
    @ColumnInfo(name="itemId")
    private String itemId;


    public Category(@NonNull String id, @NonNull String name, @NonNull String itemId) {
        this.id = id;
        this.name = name;
        this.itemId = itemId;
    }


    @NonNull
    public String getId(){ return id; }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getItemId(){
        return itemId;
    }


    @NonNull
    public void setId(String id){ this.id = id; }

    @NonNull
    public void setName(String name){ this.name = name; }

    @NonNull
    public void setItemId(String itemId){ this.itemId = itemId; }

}
