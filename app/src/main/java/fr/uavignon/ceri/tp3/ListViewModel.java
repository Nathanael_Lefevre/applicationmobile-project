package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.ItemRepository;

public class ListViewModel extends AndroidViewModel {

    private ItemRepository repository;
    private LiveData<List<Item>> allItems;


    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    private MutableLiveData<String> loadCollectionMsg;

    private static MutableLiveData<String> sortStrategy;


    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public MutableLiveData<String> getLoadCollectionMsg() { return loadCollectionMsg; }


    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }


    public void nullWebServiceThrowable() {
        webServiceThrowable.postValue(null);
    }

    public void nullLoadCollectionMsg() { loadCollectionMsg.postValue(null);}

    @NonNull
    public MutableLiveData<String> getSortStrategy() { return sortStrategy; }

    public static void setSortStrategy(String sortStrategy) {ListViewModel.sortStrategy.postValue(sortStrategy);}


    public ListViewModel (Application application) {
        super(application);
        repository = ItemRepository.get(application);
        allItems = repository.getAllItems();

        isLoading = repository.getIsLoading();
        webServiceThrowable = repository.getWebServiceThrowable();

        loadCollectionMsg = repository.getLoadCollectionMsg();

        sortStrategy = new MutableLiveData<>();
        sortStrategy.postValue("Alphabetical");
    }


    LiveData<List<Item>> getAllItems() {
        return allItems;
    }

    LiveData<List<Item>> getAllItemsFromRepo() {
        return repository.getAllItems();
    }

    public void setAllItems(MutableLiveData<List<Item>> allItems) { this.allItems = allItems; }

    public void reloadAllItemsFromRepo() {
        repository.reloadAllItemsFromBdd();
        allItems = repository.getAllItems();
    }


    public void deleteCity(String id) {
        repository.deleteItem(id);
    }


    public void loadCollectionIfNecessary(){
        repository.loadCollectionIfNecessary();
    }

}
