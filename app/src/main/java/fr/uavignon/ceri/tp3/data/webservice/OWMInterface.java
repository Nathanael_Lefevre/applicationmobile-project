package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;


public interface OWMInterface {
    /*
    @Headers("Accept: application/geo+json")
    @GET("/gridpoints/{office}/{gridX},{gridY}/forecast")
    Call<WeatherResponse> getForecast(@Path("office") String office,
                                      @Path("gridX") int gridX,
                                      @Path("gridY") int gridY);

 */

    @Headers({
            "Accept: json",
    })
    @GET("collection")
    public Call<Map<String, ItemResponse>> getCollection();

    @Headers({
            "Accept: json",
    })
    @GET("collectionversion")
    public Call<Float> getOnlineCollectionVersion();
}
