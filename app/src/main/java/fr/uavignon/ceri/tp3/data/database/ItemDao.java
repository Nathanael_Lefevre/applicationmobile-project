package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Category;
import fr.uavignon.ceri.tp3.data.CollectionVersion;
import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.ItemCTDP;
import fr.uavignon.ceri.tp3.data.Photo;
import fr.uavignon.ceri.tp3.data.TechnicalDetail;

@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("DELETE FROM item_table")
    void deleteAll();

    @Query("SELECT * from item_table ORDER BY name ASC")
    LiveData<List<Item>> getAllItems();

    @Query("SELECT * from item_table ORDER BY name ASC")
    List<Item> getSynchrAllItems();

    @Query("DELETE FROM item_table WHERE _id = :id")
    void deleteItem(String id);

    @Query("SELECT * FROM item_table WHERE _id = :id")
    Item getItemById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item item);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOrReplace(Item item);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOrReplace(Category category);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOrReplace(TechnicalDetail technicalDetail);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOrReplacePhoto(Photo photo);


    @Transaction
    @Query("SELECT * FROM item_table WHERE _id = :id")
    ItemCTDP getItemCTDById(String id);

    @Query("SELECT _id FROM collectionVersion_table")
    CollectionVersion getCollectionVersion();

    @Query("DELETE from collectionVersion_table")
    void deleteCollectionVersion();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOrReplaceCollectionVersion(CollectionVersion collectionVersion);
}
