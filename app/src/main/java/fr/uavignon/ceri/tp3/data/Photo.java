package fr.uavignon.ceri.tp3.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "photo_table", indices = {@Index(value = {"_id"},
        unique = true)})
public class Photo {

    public static final String TAG = Photo.class.getSimpleName();

    public static final String ADD_ID = "addId";

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="photoName")
    private String name;

    @ColumnInfo(name="photoInfo")
    private String info;

    @NonNull
    @ColumnInfo(name="itemId")
    private String itemId;


    public Photo(@NonNull String id, @NonNull String name, String info, @NonNull String itemId) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.itemId = itemId;
    }


    @NonNull
    public String getId(){ return id; }

    @NonNull
    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    @NonNull
    public String getItemId(){
        return itemId;
    }


    @NonNull
    public void setId(String id){ this.id = id; }

    @NonNull
    public void setName(String name){ this.name = name; }

    public void setInfo(String info) {
        this.info = info;
    }

    @NonNull
    public void setItemId(String itemId){ this.itemId = itemId; }
}
