package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CollectionRoomDatabase;
import fr.uavignon.ceri.tp3.data.database.ItemDao;
import fr.uavignon.ceri.tp3.data.webservice.ItemResponse;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.ItemResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.CollectionRoomDatabase.databaseWriteExecutor;

public class ItemRepository {

    private static final String TAG = ItemRepository.class.getSimpleName();

    private LiveData<List<Item>> allItems;
    private MutableLiveData<ItemCTDP> selectedItemCTD;

    MutableLiveData<Boolean> isLoading;
    MutableLiveData<Throwable> webServiceThrowable;
    
    private MutableLiveData<String> loadCollectionMsg;

    private ItemDao itemDao;

    private CollectionVersion localCollectionVersion;

    private CollectionVersion onlineCollectionVersion;

    private final OWMInterface api;

    private static volatile ItemRepository INSTANCE;

    public CollectionVersion getLocalCollectionVersion() {
        return localCollectionVersion;
    }

    public void setLocalCollectionVersion(CollectionVersion localCollectionVersion) {
        this.localCollectionVersion = localCollectionVersion;
    }

    public CollectionVersion getOnlineCollectionVersion() {
        return onlineCollectionVersion;
    }

    public void setOnlineCollectionVersion(CollectionVersion onlineCollectionVersion) {
        this.onlineCollectionVersion = onlineCollectionVersion;
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public MutableLiveData<String> getLoadCollectionMsg() { return loadCollectionMsg; }


    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }


    public synchronized static ItemRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ItemRepository(application);
        }
        return INSTANCE;
    }

    public ItemRepository(Application application) {
        CollectionRoomDatabase db = CollectionRoomDatabase.getDatabase(application);
        itemDao = db.itemDao();
        allItems = itemDao.getAllItems();
        selectedItemCTD = new MutableLiveData<>();

        localCollectionVersion = getCollectionVersionFromBdd();

        isLoading = new MutableLiveData<Boolean>();
        loadCollectionMsg = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<Throwable>();

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api = retrofit.create(OWMInterface.class);
    }

    public LiveData<List<Item>> getAllItems() {
        return allItems;
    }


    public MutableLiveData<ItemCTDP> getSelectedItemWithCategory() { return selectedItemCTD; }


    public Long insertOrReplaceItem(Item item) {
        Future<Long> fint = databaseWriteExecutor.submit(() -> {
            return itemDao.insertOrReplace(item);
        });
        Long res = (long) -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }


    public Long insertOrReplaceCategoryLink(Category category) {
        Future<Long> fint = databaseWriteExecutor.submit(() -> {
            return itemDao.insertOrReplace(category);
        });
        Long res = (long) -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }


    public Long insertOrReplaceTechnicalDetail(TechnicalDetail technicalDetail) {
        Future<Long> fint = databaseWriteExecutor.submit(() -> {
            return itemDao.insertOrReplace(technicalDetail);
        });
        Long res = (long) -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public Long insertOrReplacePhoto(Photo photo) {
        Future<Long> fint = databaseWriteExecutor.submit(() -> {
            return itemDao.insertOrReplacePhoto(photo);
        });
        Long res = (long) -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }


    public void deleteItem(String id) {
        databaseWriteExecutor.execute(() -> {
            itemDao.deleteItem(id);
        });
    }

    public void getItemCTD(String id)  {
        Future<ItemCTDP> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return itemDao.getItemCTDById(id);
        });
        try {
            selectedItemCTD.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void reloadAllItemsFromBdd()  {
        Future<LiveData<List<Item>>> fcity = databaseWriteExecutor.submit(() -> {
            return itemDao.getAllItems();
        });
        try {
            Log.d("isAllItemsNUll", fcity.get() == null ? "YES" : "NO");
            allItems = fcity.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private CollectionVersion getCollectionVersionFromBdd() {
        Future<CollectionVersion> fcolVers = databaseWriteExecutor.submit(() -> {
            itemDao.deleteCollectionVersion();
            return itemDao.getCollectionVersion();
        });;
        try {
            if(fcolVers.get() != null) Log.d("getCollVersFromBdd", "collVers in bdd is : " + fcolVers.get().getVersionNb());
            return fcolVers.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Long updateLocalCollectionVersion(CollectionVersion collectionVersion) {
        Future<Long> fint = databaseWriteExecutor.submit(() -> {
            itemDao.deleteCollectionVersion();
            return itemDao.insertOrReplaceCollectionVersion(collectionVersion);
        });
        Long res = (long) -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }


    public void updateLoadingState(MutableLiveData<Boolean> loadingState){
        loadingState.postValue(isLoading.getValue());
    }


    public void loadCollectionIfNecessary(){
        isLoading.setValue(true);
        api.getOnlineCollectionVersion().enqueue(
                new Callback<Float>() {  // Callback from Retrofit


                    @Override
                    public void onResponse(Call<Float> call, Response<Float> response) {

                        Log.d("Call :", call.toString());
                        Log.d("UpdateOCV", "Collection version retrived is : " + response.body());
                        if(getLocalCollectionVersion() == null || response.body().floatValue() != getLocalCollectionVersion().getVersionNb()){
                            setOnlineCollectionVersion(new CollectionVersion(response.body().floatValue()));
                            loadCollection();
                        }else{
                            loadCollectionMsg.postValue("Catalogue déjà à jour");
                            isLoading.postValue(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<Float> call, Throwable t) {
                        Log.d("Call :", call.toString());
                        Log.d("FailureLWC : ", t.getMessage());
                        System.out.println("----------------------STACK TRACE-----------------------");
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        t.printStackTrace(pw);
                        String sStackTrace = sw.toString(); // stack trace as a string
                        System.out.println(sStackTrace);
                        System.out.println("--------------------------------------------------------");

                        webServiceThrowable.postValue(t);
                        isLoading.postValue(false);
                        loadCollectionMsg.postValue("");
                    }
                });
    }

    public void loadCollection() {
        isLoading.setValue(true);

        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {  // Callback from Retrofit
                    @Override
                    public void onResponse(Call<Map<String, ItemResponse>> call,
                                           Response<Map<String, ItemResponse>> response) {
                        Log.d("Call :", call.toString());

                        for (String itemId : response.body().keySet()) {
                            Item currentItem = new Item(itemId);
                            ItemResult.transferInfo(response.body().get(itemId), currentItem);
                            insertOrReplaceItem(currentItem); // met à jour selectedCity seulement lorsqu'il s'agit de la ville affichée dans detailFragment ou quand aucune ville n'est selectionnée
                            for (String currentCategoryName : response.body().get(itemId).categories) {
                                Log.d("category add", "Adding category '" + currentCategoryName + "to item " + itemId);
                                Category categoryOfItem = new Category(currentCategoryName + itemId, currentCategoryName, itemId);
                                insertOrReplaceCategoryLink(categoryOfItem);
                            }
                            if (response.body().get(itemId).technicalDetails != null) {
                                for (String currentTechnicalDetail : response.body().get(itemId).technicalDetails) {
                                    Log.d("technicalDetail add", "Adding detail '" + currentTechnicalDetail + "to item " + itemId);
                                    TechnicalDetail technicalDetailOfItem = new TechnicalDetail(currentTechnicalDetail + itemId, currentTechnicalDetail, itemId);
                                    insertOrReplaceTechnicalDetail(technicalDetailOfItem);
                                }
                            }
                            if (response.body().get(itemId).pictures != null) {
                                for (String currentPictureName : response.body().get(itemId).pictures.keySet()) {
                                    Photo photoOfItem = new Photo(currentPictureName+itemId, currentPictureName, response.body().get(itemId).pictures.get(currentPictureName), itemId);
                                    insertOrReplacePhoto(photoOfItem);
                                }
                            }

                            if (selectedItemCTD.getValue() == null || currentItem.getId() == selectedItemCTD.getValue().item.getId())
                                getItemCTD(currentItem.getId());
                        }

                        // mise à jour du numéro de version local
                        setLocalCollectionVersion(new CollectionVersion(getOnlineCollectionVersion().getVersionNb()));
                        updateLocalCollectionVersion(getLocalCollectionVersion());
                        isLoading.postValue(false);
                        loadCollectionMsg.postValue("Nouvelle version : " + getLocalCollectionVersion().getVersionNb());
                    }

                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                        Log.d("Call :", call.toString());
                        Log.d("FailureLWC : ", t.getMessage());
                        System.out.println("----------------------STACK TRACE-----------------------");
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        t.printStackTrace(pw);
                        String sStackTrace = sw.toString(); // stack trace as a string
                        System.out.println(sStackTrace);
                        System.out.println("--------------------------------------------------------");

                        webServiceThrowable.postValue(t);
                        isLoading.postValue(false);
                        loadCollectionMsg.postValue("");
                    }
                });


    }


    /*
    public void loadWeatherAllCities(){
        List<Item> synchrAllCities = getSynchrAllCities();
        nbAPIloads = synchrAllCities.size();

        for (Item item : synchrAllCities) {
            loadCollection(item);
        }
    }
    */

}
