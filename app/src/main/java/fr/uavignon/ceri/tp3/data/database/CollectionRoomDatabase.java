package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.Category;
import fr.uavignon.ceri.tp3.data.CollectionVersion;
import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.Photo;
import fr.uavignon.ceri.tp3.data.TechnicalDetail;

@Database(entities = {Item.class, Category.class, TechnicalDetail.class, Photo.class, CollectionVersion.class}, version = 10, exportSchema = false)
public abstract class CollectionRoomDatabase extends RoomDatabase {

    private static final String TAG = CollectionRoomDatabase.class.getSimpleName();

    public abstract ItemDao itemDao();

    private static CollectionRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static CollectionRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CollectionRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                            // without populate
                        /*
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    WeatherRoomDatabase.class,"book_database")
                                    .build();

                     */

                            // with populate
                            INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    CollectionRoomDatabase.class,"item_database")
                                    .fallbackToDestructiveMigration()
                                    .build();

                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        ItemDao dao = INSTANCE.itemDao();
                        dao.deleteAll();

                        /*
                        Item[] cities = {new Item("Avignon", "France"),
                        new Item("Paris", "France"),
                        new Item("Rennes", "France"),
                        new Item("Montreal", "Canada"),
                        new Item("Rio de Janeiro", "Brazil"),
                        new Item("Papeete", "French Polynesia"),
                        new Item("Sydney", "Australia"),
                        new Item("Seoul", "South Korea"),
                        new Item("Bamako", "Mali"),
                        new Item("Istanbul", "Turkey")};

                        for(Item newItem : cities)
                            dao.insert(newItem);
                        Log.d(TAG,"database populated");

                         */
                    });

                }
            };



}
