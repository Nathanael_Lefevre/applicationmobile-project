package fr.uavignon.ceri.tp3.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "technicalDetail_table", indices = {@Index(value = {"_id"},
        unique = true)})
public class TechnicalDetail {

    public static final String TAG = TechnicalDetail.class.getSimpleName();

    public static final String ADD_ID = "addId";

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="catName")
    private String detail;

    @NonNull
    @ColumnInfo(name="itemId")
    private String itemId;


    public TechnicalDetail(@NonNull String id, @NonNull String detail, @NonNull String itemId) {
        this.id = id;
        this.detail = detail;
        this.itemId = itemId;
    }


    @NonNull
    public String getId(){ return id; }

    @NonNull
    public String getDetail() {
        return detail;
    }

    @NonNull
    public String getItemId(){
        return itemId;
    }


    @NonNull
    public void setId(String id){ this.id = id; }

    @NonNull
    public void setDetail(String detail){ this.detail = detail; }

    @NonNull
    public void setItemId(String itemId){ this.itemId = itemId; }

}
