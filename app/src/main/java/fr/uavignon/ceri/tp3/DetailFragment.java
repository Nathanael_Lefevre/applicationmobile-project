package fr.uavignon.ceri.tp3;

import android.database.MatrixCursor;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp3.data.Category;
import fr.uavignon.ceri.tp3.data.ItemCTDP;
import fr.uavignon.ceri.tp3.data.TechnicalDetail;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textItemName, textDescription, textBrand, textYear, textCategory, textLastUpdate;
    private TextView labelTechnicalDetails;
    private ImageView imgItem;
    private ProgressBar progress;
    private TableLayout tableTechnicalDetails;

    private int numImage;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String itemID = args.getItemNum();
        Log.d(TAG,"selected id="+itemID);
        viewModel.setItem(itemID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textItemName = getView().findViewById(R.id.nameItem);
        textDescription = getView().findViewById(R.id.editDescription);
        textBrand = getView().findViewById(R.id.editBrand);
        textYear = getView().findViewById(R.id.editYear);
        textCategory = getView().findViewById(R.id.editCategory);

        labelTechnicalDetails = getView().findViewById(R.id.labelTechnicalDetails);

        textLastUpdate = getView().findViewById(R.id.editLastUpdate);
        imgItem = getView().findViewById(R.id.iconeItem);
        numImage = 0;

        tableTechnicalDetails = (TableLayout) getView().findViewById(R.id.idTable);

        progress = getView().findViewById(R.id.progress);

        getView().findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                Snackbar.make(view, "Mise à jour des informations",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                 */
                viewModel.loadCollectionIfNecessary();
            }
        });

        getView().findViewById(R.id.iconeItem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ItemCTDP itemCTDP = viewModel.getItemWithCategory().getValue();
                if(itemCTDP.pictures != null && itemCTDP.pictures.size() != 0) {
                    String imgUrl = "";
                    numImage = numImage == itemCTDP.pictures.size() - 1 ? 0 : numImage + 1;

                    Snackbar.make(view, "Image suivante : " + itemCTDP.pictures.get(numImage).getInfo(),
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    imgUrl = "https://demo-lia.univ-avignon.fr/cerimuseum/items/"+itemCTDP.item.getId()+"/images/"+itemCTDP.pictures.get(numImage).getName();
                    Glide.with(getActivity())
                            .load(imgUrl)
                            .placeholder(R.drawable.progress_bar)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(imgItem);
                }

            }
        });

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getItemWithCategory().observe(getViewLifecycleOwner(),
                itemWithCategory -> {
                    if (itemWithCategory != null) {
                        Log.d(TAG, "observing Item view");

                        textItemName.setText(itemWithCategory.item.getName());
                        textDescription.setText(itemWithCategory.item.getDescription());
                        textBrand.setText(itemWithCategory.item.getBrand());

                        textYear.setText(itemWithCategory.item.getYear() != -1 ? String.valueOf(itemWithCategory.item.getYear()) : "????");

                        textLastUpdate.setText(itemWithCategory.item.getStrLastUpdate());

                        try {
                            textCategory.setText(itemWithCategory.categories.get(0).getName());
                            for (Category currentCategory : itemWithCategory.categories.subList(1, itemWithCategory.categories.size())){
                                Log.d("categoryRead", "Category read is " + currentCategory.getName());
                                textCategory.setText(textCategory.getText() + ", " + currentCategory.getName());
                            }
                        } catch(Throwable e){
                            textCategory.setText("");
                        }


                        TableRow row; // création d'un élément : ligne
                        TextView tv1; // création des cellules

                        // pour chaque ligne
                        if(itemWithCategory.technicalDetails.size() == 0){
                            labelTechnicalDetails.setText("");
                            labelTechnicalDetails.setHeight(0);
                        } else {
                            for(int i=0;i<itemWithCategory.technicalDetails.size();i++) {
                                row = new TableRow(this.getActivity()); // création d'une nouvelle ligne

                                //tv1 = new TextView(this.getActivity()); // création cellule
                                tv1 = (TextView) getLayoutInflater().inflate(R.layout.table_row_layout, null);
                                tv1.setText(itemWithCategory.technicalDetails.get(i).getDetail()); // ajout du texte
                                tv1.setGravity(Gravity.CENTER); // centrage dans la cellule
                                // adaptation de la largeur de colonne à l'écran :
                                tv1.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1));
                                tv1.setLayoutParams(new TableRow.LayoutParams());

                                // ajout des cellules à la ligne
                                row.addView(tv1);

                                // ajout de la ligne au tableau
                                tableTechnicalDetails.addView(row);
                            }
                        }


                        String imgUrl = "";
                        if(itemWithCategory.pictures != null && itemWithCategory.pictures.size() != 0) {
                            imgUrl = "https://demo-lia.univ-avignon.fr/cerimuseum/items/"+itemWithCategory.item.getId()+"/images/"+itemWithCategory.pictures.get(0).getName();
                        }else{
                            imgUrl = "https://demo-lia.univ-avignon.fr/cerimuseum//items/"+itemWithCategory.item.getId()+"/thumbnail";
                        }

                        Glide.with(this.getActivity())
                                .load(imgUrl)
                                .placeholder(R.drawable.progress_bar)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .into(imgItem);

                        /*
                        // set ImgView
                        if (city.getIconUri() != null)
                            imgItem.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(city.getIconUri(),
                                null, getContext().getPackageName())));

                         */

                    }
                });

        viewModel.getIsLoading().observe(getViewLifecycleOwner(),
                bool -> {
                    if (bool != null) {
                        Log.d(TAG, "Loading state has just been updated : " + bool.toString());
                        if (bool) {
                            System.out.println("setting visibility of progressBar to " + View.VISIBLE);
                            progress.setVisibility(View.VISIBLE);
                        } else {
                            progress.setVisibility(View.GONE);
                            System.out.println("setting visibility of progressBar to " + View.GONE);
                        }
                    }
                });

        viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                throwable -> {
                    if (throwable != null && throwable.getMessage() != null){
                        Snackbar.make(getView(), "Erreur : " + throwable.getMessage(),
                                Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        viewModel.nullWebServiceThrowable();  // Remet le WebServiceThrowable à null pour ne pas l'afficher à nouveau en changeant de fragment
                    }
                });
    }


}